package nl.aimsites.nzbvuq.bootstrapper;

import java.util.List;

/**
 * The type Bootstrap handler.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class BootstrapHandler {
  private final BootstrapCache bootstrapCache;

  /**
   * Instantiates a new Bootstrap handler.
   *
   * @param bootstrapCache the bootstrap cache
   */
  public BootstrapHandler(BootstrapCache bootstrapCache) {
    this.bootstrapCache = bootstrapCache;
  }

  /**
   * Gets list of peers that connected within the last 10 minutes. It does not check if the
   * connected peers are still online, that is the responsibility of the Client.
   *
   * @param parameters String-list with the requesting identifier as its first parameter
   * @return the list of peers
   */
  public List<String> getPeers(List<String> parameters) {
    return this.bootstrapCache.getList(parameters.get(0));
  }
}
