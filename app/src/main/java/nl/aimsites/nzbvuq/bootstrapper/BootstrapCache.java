package nl.aimsites.nzbvuq.bootstrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

/**
 * The type Bootstrap cache.
 *
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 */
public class BootstrapCache {
  private final ConcurrentMap<String, Date> map;

  /**
   * Instantiates a new Bootstrap cache.
   *
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public BootstrapCache() {
    this.map = new ConcurrentHashMap<>();
  }

  /**
   * Adds the current identifier to the list, then removes all items older than 10 minutes
   *
   * @return the list
   * @author Inze Bielderman (i.bielderman@student.han.nl)
   */
  public List<String> getList(String identifier) {
    Date now = new Date();
    Date tenMinutesAgo = new Date(now.getTime() - TimeUnit.MINUTES.toMillis(10));

    this.map.put(identifier, now);
    this.map.entrySet().removeIf(entry -> entry.getValue().before(tenMinutesAgo));

    return new ArrayList<>(map.keySet());
  }
}
