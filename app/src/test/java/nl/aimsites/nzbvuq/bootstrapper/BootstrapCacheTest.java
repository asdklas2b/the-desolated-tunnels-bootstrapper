package nl.aimsites.nzbvuq.bootstrapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

/** @author Inze Bielderman (i.bielderman@student.han.nl) */
class BootstrapCacheTest {
  BootstrapCache bootstrapCache;

  @BeforeEach
  void setUp() {
    bootstrapCache = new BootstrapCache();
  }

  @Test
  void getList() {
    String identifier = "identifier";

    List<String> expected = List.of(identifier);

    List<String> actual = bootstrapCache.getList(identifier);

    assertArrayEquals(expected.toArray(), actual.toArray());
  }
}
