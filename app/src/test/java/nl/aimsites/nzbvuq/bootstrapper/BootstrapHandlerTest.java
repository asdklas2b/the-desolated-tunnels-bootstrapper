package nl.aimsites.nzbvuq.bootstrapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Unit Tests for the BootstrapHandler.
 *
 * @author Fedor Soffers (fka.soffers@student.han.nl)
 * @author Inze Bielderman (i.bielderman@student.han.nl)
 */
class BootstrapHandlerTest {
  @Mock BootstrapCache bootstrapCache;
  BootstrapHandler bootstrapHandler;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    bootstrapHandler = new BootstrapHandler(bootstrapCache);
  }

  @Test
  void getPeers() {
    String identifier = "identifier";
    List<String> parameters = List.of(identifier);
    List<String> list = List.of(identifier);

    when(bootstrapCache.getList(identifier)).thenReturn(list);

    List<String> actual = bootstrapHandler.getPeers(parameters);

    assertArrayEquals(list.toArray(), actual.toArray());
    verify(bootstrapCache, times(1)).getList(identifier);
  }
}
